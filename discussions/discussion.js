
// MongoDB Operations
// For creating or inserting one data into database.
//I.
db.users.insert({
    firstName: "Ely",
    lastName: "Buendia",
    age: 50,
    contact: {
        phone: "534776",
        email: "ely@eraserheads.com"
        },
        counrses: ["CSS", "Javascript", "Phython"],
        department: "none"
})

// for insert two or more or many documents.
db.users.insertMany([
	{
		firstName: "Chito",
		lastName: "Miranda",
		age: 43,
		contact: {
			phone: "5347786",
			email: "chito@parokya.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Francis",
		lastName: "Magalona",
		age: 61,
		contact: {
			phone: "5347786",
			email: "francisM@email.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "none"
	}
])


// ===========================================

//II. 
// for queying all the data in database.
// For finding documents.

// FIND ALL
db.users.find();

// FIND [One]
db.users.find({firstName: "Francis", age: 61});

// ==========================================

// III.
// How to delete
db.users.deleteOne({
	firstName: "Ely"
});

// How to delete all
db.sers.deleteMany ();

// ============================= 

// for UPDATE
$set: {

} 
// IV. (A.)
// for examples of UPDATE
db.users.updateOne(
{
    lastName: "Meranda"
},
{
    $set: {
        lastName: "Esguerra"
    }
}
);

// ====================================
// ModifiedCount means bibilangin niya kung ilan yung na modified.



// ==========================================
// Pwede rin rekta. I mean pwede d na mag open ng shell
// for example
db.users.insert(
	{
		firstName: "Gloc-9",
		lastName: "Walang Apelyido",
		age: 43,
		contact: {
			phone: "5336234",
			email: "gloc@parokya.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	}

);
        
db.users.find();
// ===============================================


// Delete Many
db.users.deleteMany({
	department: "DOH"
});

// ===============================================
// IV. (B.)
// Update Many
db.users.updateMany(
    {
        department: "none"
    },
    {
        $set: {
            department: "HR"
        }
    }
);
// click play then go to db.users.find(); and click play button.
// Check and see if your department is changes into HR.
// ===========================================